class RPNCalculator
  attr_accessor :calculator

  def initialize
    @calculator = []
  end

  def push(num)
    @calculator << num
  end

  def plus
    perform_function(:+)
  end

  def minus
    perform_function(:-)
  end

  def divide
    @calculator.map!(&:to_f)
    perform_function(:/)
  end

  def times
    perform_function(:*)
  end

  def perform_function(symbol)
    raise "calculator is empty" if @calculator.length < 2
    nums_to_reduce = @calculator.pop(2)
    @calculator << nums_to_reduce.reduce(symbol)
  end

  def value
    @calculator.last
  end
end
